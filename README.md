# Projet Docker

## Info
- Le projet dockerisé est un projet Symfony sous la forme d'un forum
- Tois conteneur son utilisé : Mysql, phpMyAdmin et php
- Il est possible de créer des "Topic" et des "Post" dans les sujet du forum
- On peux enregistrer un compte sur le forum
- Deux utilisateurs sont deja pré-enregistré : Utilisateur (login) / azerty45 (mdp) et Administrateur (login) / azerty45 (mdp)

## Installation

Dans le dossier docker ouvrir un windows powershell

Saisir la commande suivante:

```shell
docker-compose up -d
```
Une fois l'image et les container créée saisir la commande :
```shell
docker exec -it www_docker_symfony bash
```
puis

```shell
cd project/
```

et enfin:

```shell
composer install
```

Une fois les commande saisie rendez vous a l'adresse phpMyAdmin : http://localhost:8080/
Utilisateur : root
Mot de Passe:

Créer une nouvelle base de donnée nommez "forum_symfony"

Une fois la base créer importez y le fichier SQL "forum_symfony.sql" dans le dossier ressource du projet.

Rendez vous maintenant à l'adresse du site symfony : http://localhost:8741/
