<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* forum/message.html.twig */
class __TwigTemplate_81bfafb9ee0debed880b0f83a575f79f0d8e92ee630147aa8243c2c36b255f25 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/message.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/message.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "forum/message.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Messages";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <div class=\"container\">
        ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["topics"]) || array_key_exists("topics", $context) ? $context["topics"] : (function () { throw new RuntimeError('Variable "topics" does not exist.', 7, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["topic"]) {
            // line 8
            echo "        <a type=\"button\" class=\"btn btn-sm btn-outline-secondary\" href=\"/categorie/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "getIdCate", [], "any", false, false, false, 8), "html", null, true);
            echo "\">Retour</a>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['topic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["topics"]) || array_key_exists("topics", $context) ? $context["topics"] : (function () { throw new RuntimeError('Variable "topics" does not exist.', 10, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["topic"]) {
            // line 11
            echo "        <div class=\"message\">
            <div class=\"jumbotron border bg-light \">
                <div class=\"container lg-12\">
                    <div class=\"float-right\">
                        <small class=\"text-muted\">
                            <button class=\"btn\">";
            // line 16
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new RuntimeError('Variable "messages" does not exist.', 16, $this->source); })())), "html", null, true);
            echo " <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-chat-dots\" viewBox=\"0 0 16 16\">
                                    <path d=\"M5 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2z\"/>
                                    <path d=\"M2.165 15.803l.02-.004c1.83-.363 2.948-.842 3.468-1.105A9.06 9.06 0 0 0 8 15c4.418 0 8-3.134 8-7s-3.582-7-8-7-8 3.134-8 7c0 1.76.743 3.37 1.97 4.6a10.437 10.437 0 0 1-.524 2.318l-.003.011a10.722 10.722 0 0 1-.244.637c-.079.186.074.394.273.362a21.673 21.673 0 0 0 .693-.125zm.8-3.108a1 1 0 0 0-.287-.801C1.618 10.83 1 9.468 1 8c0-3.192 3.004-6 7-6s7 2.808 7 6c0 3.193-3.004 6-7 6a8.06 8.06 0 0 1-2.088-.272 1 1 0 0 0-.711.074c-.387.196-1.24.57-2.634.893a10.97 10.97 0 0 0 .398-2z\"/>
                                </svg></button>
                        </small>
                    </div>
                    <div class=\"d-flex flex-column\">
                        <p class=\"h4\">";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "getTitreTopic", [], "method", false, false, false, 23), "html", null, true);
            echo "</p>
                        <small>";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "getPseudoUser", [], "method", false, false, false, 24), "html", null, true);
            echo "</small>
                        <small class=\"text-muted\">le ";
            // line 25
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "getDateHeureTopic", [], "method", false, false, false, 25), "m/d/Y"), "html", null, true);
            echo " à ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "getDateHeureTopic", [], "any", false, false, false, 25), "H:i"), "html", null, true);
            echo "<br><br></small>
                        <p>
                            ";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "getContenuTopic", [], "any", false, false, false, 27), "html", null, true);
            echo "
                        </p>
                    </div>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['topic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new RuntimeError('Variable "messages" does not exist.', 33, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 34
            echo "                <div class=\"jumbotron border bg-light \">
                    <div class=\"container lg-12\">
                        <small class=\"float-right\">
                            ";
            // line 37
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 37), "username", [], "any", true, true, false, 37)) {
                // line 38
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 38, $this->source); })()), "user", [], "any", false, false, false, 38), "roles", [], "any", false, false, false, 38));
                foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                    // line 39
                    echo "                                    ";
                    if ((0 === twig_compare($context["role"], "ROLE_ADMIN"))) {
                        // line 40
                        echo "                                        <small class=\"text-muted\">
                                            <a class=\"btn\" href=\"";
                        // line 41
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_supprimer_message", ["id" => twig_get_attribute($this->env, $this->source, $context["message"], "id", [], "any", false, false, false, 41)]), "html", null, true);
                        echo "\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-trash\" viewBox=\"0 0 16 16\">
                                                    <path d=\"M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z\"/>
                                                    <path fill-rule=\"evenodd\" d=\"M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z\"/>
                                                </svg>
                                            </a>
                                        </small>
                                    ";
                    }
                    // line 48
                    echo "                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 49
                echo "                            ";
            }
            // line 50
            echo "                        </small>
                        <div class=\"d-flex flex-column\">
                            <small>";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["message"], "getPseudoUser", [], "method", false, false, false, 52), "html", null, true);
            echo "</small>
                            <small class=\"text-muted\">le ";
            // line 53
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["message"], "getDateHeureMess", [], "method", false, false, false, 53), "m/d/Y"), "html", null, true);
            echo " à ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["message"], "getDateHeureMess", [], "any", false, false, false, 53), "H:i"), "html", null, true);
            echo "<br><br></small>
                            <p>";
            // line 54
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["message"], "getContenuMess", [], "method", false, false, false, 54), "html", null, true);
            echo "</p>
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "            ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 59), "username", [], "any", true, true, false, 59)) {
            // line 60
            echo "                <div class=\"jumbotron border bg-light \">
                    <div class=\"container lg-12\">
                        ";
            // line 62
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formMessage"]) || array_key_exists("formMessage", $context) ? $context["formMessage"] : (function () { throw new RuntimeError('Variable "formMessage" does not exist.', 62, $this->source); })()), 'form_start');
            echo "
                            <h5>Répondre au sujet</h5>
                            ";
            // line 64
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formMessage"]) || array_key_exists("formMessage", $context) ? $context["formMessage"] : (function () { throw new RuntimeError('Variable "formMessage" does not exist.', 64, $this->source); })()), "contenuMess", [], "any", false, false, false, 64), 'widget', ["attr" => ["id" => "titre", "placeholder" => "Votre message", "maxlength" => "100", "minlength" => "2"]]);
            echo "
                            
                            <small id=\"emailHelp\" class=\"form-text text-muted\">500 caractères max.</small>

                            <button type=\"submit\" class=\"btn btn btn-primary\">Envoyer</button>
                        ";
            // line 69
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formMessage"]) || array_key_exists("formMessage", $context) ? $context["formMessage"] : (function () { throw new RuntimeError('Variable "formMessage" does not exist.', 69, $this->source); })()), 'form_end');
            echo "
                    </div>
                </div>
            ";
        }
        // line 73
        echo "        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "forum/message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  247 => 73,  240 => 69,  232 => 64,  227 => 62,  223 => 60,  220 => 59,  209 => 54,  203 => 53,  199 => 52,  195 => 50,  192 => 49,  186 => 48,  176 => 41,  173 => 40,  170 => 39,  165 => 38,  163 => 37,  158 => 34,  153 => 33,  141 => 27,  134 => 25,  130 => 24,  126 => 23,  116 => 16,  109 => 11,  104 => 10,  95 => 8,  91 => 7,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Messages{% endblock %}

{% block content %}
    <div class=\"container\">
        {% for topic in topics %}
        <a type=\"button\" class=\"btn btn-sm btn-outline-secondary\" href=\"/categorie/{{ topic.getIdCate }}\">Retour</a>
        {% endfor %}
        {% for topic in topics %}
        <div class=\"message\">
            <div class=\"jumbotron border bg-light \">
                <div class=\"container lg-12\">
                    <div class=\"float-right\">
                        <small class=\"text-muted\">
                            <button class=\"btn\">{{ messages | length }} <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-chat-dots\" viewBox=\"0 0 16 16\">
                                    <path d=\"M5 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2z\"/>
                                    <path d=\"M2.165 15.803l.02-.004c1.83-.363 2.948-.842 3.468-1.105A9.06 9.06 0 0 0 8 15c4.418 0 8-3.134 8-7s-3.582-7-8-7-8 3.134-8 7c0 1.76.743 3.37 1.97 4.6a10.437 10.437 0 0 1-.524 2.318l-.003.011a10.722 10.722 0 0 1-.244.637c-.079.186.074.394.273.362a21.673 21.673 0 0 0 .693-.125zm.8-3.108a1 1 0 0 0-.287-.801C1.618 10.83 1 9.468 1 8c0-3.192 3.004-6 7-6s7 2.808 7 6c0 3.193-3.004 6-7 6a8.06 8.06 0 0 1-2.088-.272 1 1 0 0 0-.711.074c-.387.196-1.24.57-2.634.893a10.97 10.97 0 0 0 .398-2z\"/>
                                </svg></button>
                        </small>
                    </div>
                    <div class=\"d-flex flex-column\">
                        <p class=\"h4\">{{ topic.getTitreTopic() }}</p>
                        <small>{{ topic.getPseudoUser() }}</small>
                        <small class=\"text-muted\">le {{ topic.getDateHeureTopic()|date(\"m/d/Y\") }} à {{topic.getDateHeureTopic|date(\"H:i\")}}<br><br></small>
                        <p>
                            {{ topic.getContenuTopic }}
                        </p>
                    </div>
                </div>
            </div>
        {% endfor %}
            {% for message in messages %}
                <div class=\"jumbotron border bg-light \">
                    <div class=\"container lg-12\">
                        <small class=\"float-right\">
                            {% if app.user.username is defined %}
                                {% for role in app.user.roles %}
                                    {% if role == \"ROLE_ADMIN\" %}
                                        <small class=\"text-muted\">
                                            <a class=\"btn\" href=\"{{ path('admin_supprimer_message', {'id':message.id}) }}\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-trash\" viewBox=\"0 0 16 16\">
                                                    <path d=\"M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z\"/>
                                                    <path fill-rule=\"evenodd\" d=\"M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z\"/>
                                                </svg>
                                            </a>
                                        </small>
                                    {% endif %}
                                {% endfor %}
                            {% endif %}
                        </small>
                        <div class=\"d-flex flex-column\">
                            <small>{{ message.getPseudoUser() }}</small>
                            <small class=\"text-muted\">le {{ message.getDateHeureMess()|date(\"m/d/Y\")}} à {{message.getDateHeureMess|date(\"H:i\")}}<br><br></small>
                            <p>{{ message.getContenuMess() }}</p>
                        </div>
                    </div>
                </div>
            {% endfor %}
            {% if app.user.username is defined %}
                <div class=\"jumbotron border bg-light \">
                    <div class=\"container lg-12\">
                        {{  form_start(formMessage) }}
                            <h5>Répondre au sujet</h5>
                            {{ form_widget(formMessage.contenuMess, {'attr': {'id': \"titre\", 'placeholder': \"Votre message\", 'maxlength': \"100\", 'minlength': \"2\"}})}}
                            
                            <small id=\"emailHelp\" class=\"form-text text-muted\">500 caractères max.</small>

                            <button type=\"submit\" class=\"btn btn btn-primary\">Envoyer</button>
                        {{  form_end(formMessage) }}
                    </div>
                </div>
            {% endif %}
        </div>
    </div>
{% endblock %}", "forum/message.html.twig", "D:\\CoursLP\\Dockers\\projet\\Projet\\project\\templates\\forum\\message.html.twig");
    }
}
