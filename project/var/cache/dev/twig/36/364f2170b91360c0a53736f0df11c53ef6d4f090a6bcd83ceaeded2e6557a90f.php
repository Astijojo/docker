<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* forum/categorie.html.twig */
class __TwigTemplate_0ee76f6b233edbc3f2f4cd01cae8632a774a5801d55f962236f9732662b77ea9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/categorie.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "forum/categorie.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "forum/categorie.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Sujets";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <div class=\"container\">
        <a type=\"button\" class=\"btn btn-sm btn-outline-secondary\" href=\"/\">Retour</a>
        <div class=\"topics\">
            ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["topics"]) || array_key_exists("topics", $context) ? $context["topics"] : (function () { throw new RuntimeError('Variable "topics" does not exist.', 9, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["topic"]) {
            // line 10
            echo "            <div class=\"jumbotron border bg-light \">
                <div class=\"container lg-12\">
                    <div class=\"float-right\">
                        ";
            // line 13
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 13), "username", [], "any", true, true, false, 13)) {
                // line 14
                echo "                            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 14, $this->source); })()), "user", [], "any", false, false, false, 14), "roles", [], "any", false, false, false, 14));
                foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                    // line 15
                    echo "                                ";
                    if ((0 === twig_compare($context["role"], "ROLE_ADMIN"))) {
                        // line 16
                        echo "                                    <small class=\"text-muted\">
                                        <a class=\"btn\" href=\"";
                        // line 17
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_supprimer_topic", ["id" => twig_get_attribute($this->env, $this->source, $context["topic"], "id", [], "any", false, false, false, 17)]), "html", null, true);
                        echo "\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-trash\" viewBox=\"0 0 16 16\">
                                                <path d=\"M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z\"/>
                                                <path fill-rule=\"evenodd\" d=\"M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z\"/>
                                            </svg>
                                        </a>
                                    </small>
                                ";
                    }
                    // line 24
                    echo "                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 25
                echo "                    ";
            }
            // line 26
            echo "                    </div>
                    <a class=\"h4\" href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("topic", ["idTopic" => twig_get_attribute($this->env, $this->source, $context["topic"], "getId", [], "method", false, false, false, 27)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "getTitreTopic", [], "method", false, false, false, 27), "html", null, true);
            echo "</a>
                    <p class=\"h5\">";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "getContenuTopic", [], "any", false, false, false, 28), "html", null, true);
            echo "</p>
                    <p>";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "getPseudoUser", [], "method", false, false, false, 29), "html", null, true);
            echo " le ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "getDateHeureTopic", [], "any", false, false, false, 29), "m/d/Y"), "html", null, true);
            echo " à ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["topic"], "getDateHeureTopic", [], "any", false, false, false, 29), "H:i"), "html", null, true);
            echo "</p>
                </div>

            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['topic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "            ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 34), "username", [], "any", true, true, false, 34)) {
            // line 35
            echo "                <div class=\"jumbotron border bg-light\">
                    <div class=\"container lg-12\">
                    ";
            // line 37
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formTopic"]) || array_key_exists("formTopic", $context) ? $context["formTopic"] : (function () { throw new RuntimeError('Variable "formTopic" does not exist.', 37, $this->source); })()), 'form_start');
            echo "
                        <h5>Créer un nouveau sujet</h5>

                        ";
            // line 40
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formTopic"]) || array_key_exists("formTopic", $context) ? $context["formTopic"] : (function () { throw new RuntimeError('Variable "formTopic" does not exist.', 40, $this->source); })()), "titreTopic", [], "any", false, false, false, 40), 'widget', ["attr" => ["id" => "titre", "maxlength" => "100", "minlength" => "2", "placeholder" => "Votre titre"]]);
            echo "
                        <small id=\"emailHelp\" class=\"form-text text-muted\">100 caractères max.</small>

                        ";
            // line 43
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formTopic"]) || array_key_exists("formTopic", $context) ? $context["formTopic"] : (function () { throw new RuntimeError('Variable "formTopic" does not exist.', 43, $this->source); })()), "contenuTopic", [], "any", false, false, false, 43), 'widget', ["attr" => ["class" => "form-control", "cols" => "150", "rows" => "5", "placeholder" => "Votre message", "maxlength" => "500", "minlength" => "2"]]);
            echo "
                        <small id=\"emailHelp\" class=\"form-text text-muted\">500 caractères max.</small>

                        <button type=\"submit\" class=\"btn btn btn-primary\">Envoyer</button>

                    ";
            // line 48
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formTopic"]) || array_key_exists("formTopic", $context) ? $context["formTopic"] : (function () { throw new RuntimeError('Variable "formTopic" does not exist.', 48, $this->source); })()), 'form_end');
            echo "
                    </div>
                </div>
            ";
        }
        // line 52
        echo "        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "forum/categorie.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 52,  189 => 48,  181 => 43,  175 => 40,  169 => 37,  165 => 35,  162 => 34,  147 => 29,  143 => 28,  137 => 27,  134 => 26,  131 => 25,  125 => 24,  115 => 17,  112 => 16,  109 => 15,  104 => 14,  102 => 13,  97 => 10,  93 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Sujets{% endblock %}

{% block content %}
    <div class=\"container\">
        <a type=\"button\" class=\"btn btn-sm btn-outline-secondary\" href=\"/\">Retour</a>
        <div class=\"topics\">
            {% for topic in topics %}
            <div class=\"jumbotron border bg-light \">
                <div class=\"container lg-12\">
                    <div class=\"float-right\">
                        {% if app.user.username is defined %}
                            {% for role in app.user.roles %}
                                {% if role == \"ROLE_ADMIN\" %}
                                    <small class=\"text-muted\">
                                        <a class=\"btn\" href=\"{{ path('admin_supprimer_topic', {'id':topic.id}) }}\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-trash\" viewBox=\"0 0 16 16\">
                                                <path d=\"M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z\"/>
                                                <path fill-rule=\"evenodd\" d=\"M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z\"/>
                                            </svg>
                                        </a>
                                    </small>
                                {% endif %}
                        {% endfor %}
                    {% endif %}
                    </div>
                    <a class=\"h4\" href=\"{{ path('topic', {'idTopic': topic.getId()}) }}\">{{ topic.getTitreTopic() }}</a>
                    <p class=\"h5\">{{topic.getContenuTopic}}</p>
                    <p>{{ topic.getPseudoUser() }} le {{topic.getDateHeureTopic|date(\"m/d/Y\")}} à {{topic.getDateHeureTopic|date(\"H:i\")}}</p>
                </div>

            </div>
            {% endfor %}
            {% if app.user.username is defined %}
                <div class=\"jumbotron border bg-light\">
                    <div class=\"container lg-12\">
                    {{  form_start(formTopic) }}
                        <h5>Créer un nouveau sujet</h5>

                        {{  form_widget(formTopic.titreTopic, {'attr': {'id': \"titre\",  'maxlength': \"100\", 'minlength': \"2\", 'placeholder': \"Votre titre\"}}) }}
                        <small id=\"emailHelp\" class=\"form-text text-muted\">100 caractères max.</small>

                        {{  form_widget(formTopic.contenuTopic, {'attr': {'class': \"form-control\", 'cols': \"150\", 'rows': \"5\", 'placeholder': \"Votre message\", 'maxlength': \"500\", 'minlength': \"2\"}}) }}
                        <small id=\"emailHelp\" class=\"form-text text-muted\">500 caractères max.</small>

                        <button type=\"submit\" class=\"btn btn btn-primary\">Envoyer</button>

                    {{  form_end(formTopic) }}
                    </div>
                </div>
            {% endif %}
        </div>
    </div>
{% endblock %}", "forum/categorie.html.twig", "D:\\CoursLP\\Dockers\\projet\\Projet\\project\\templates\\forum\\categorie.html.twig");
    }
}
