<?php

namespace ContainerBiKbZDr;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolderc5634 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer51334 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties26a02 = [
        
    ];

    public function getConnection()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getConnection', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getMetadataFactory', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getExpressionBuilder', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'beginTransaction', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getCache', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getCache();
    }

    public function transactional($func)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'transactional', array('func' => $func), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->transactional($func);
    }

    public function commit()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'commit', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->commit();
    }

    public function rollback()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'rollback', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getClassMetadata', array('className' => $className), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'createQuery', array('dql' => $dql), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'createNamedQuery', array('name' => $name), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'createQueryBuilder', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'flush', array('entity' => $entity), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'clear', array('entityName' => $entityName), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->clear($entityName);
    }

    public function close()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'close', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->close();
    }

    public function persist($entity)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'persist', array('entity' => $entity), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'remove', array('entity' => $entity), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'refresh', array('entity' => $entity), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'detach', array('entity' => $entity), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'merge', array('entity' => $entity), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getRepository', array('entityName' => $entityName), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'contains', array('entity' => $entity), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getEventManager', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getConfiguration', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'isOpen', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getUnitOfWork', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getProxyFactory', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'initializeObject', array('obj' => $obj), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'getFilters', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'isFiltersStateClean', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'hasFilters', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return $this->valueHolderc5634->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer51334 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolderc5634) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderc5634 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolderc5634->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, '__get', ['name' => $name], $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        if (isset(self::$publicProperties26a02[$name])) {
            return $this->valueHolderc5634->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc5634;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderc5634;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc5634;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderc5634;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, '__isset', array('name' => $name), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc5634;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderc5634;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, '__unset', array('name' => $name), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc5634;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderc5634;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, '__clone', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        $this->valueHolderc5634 = clone $this->valueHolderc5634;
    }

    public function __sleep()
    {
        $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, '__sleep', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;

        return array('valueHolderc5634');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer51334 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer51334;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer51334 && ($this->initializer51334->__invoke($valueHolderc5634, $this, 'initializeProxy', array(), $this->initializer51334) || 1) && $this->valueHolderc5634 = $valueHolderc5634;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderc5634;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderc5634;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
