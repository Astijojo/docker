-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : db
-- Généré le : lun. 08 fév. 2021 à 13:47
-- Version du serveur :  8.0.23
-- Version de PHP : 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `forum_symfony`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int NOT NULL,
  `nom_cate` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom_cate`, `created_at`) VALUES
(1, 'Sport', '2021-01-12 23:02:50'),
(2, 'Cours', '2021-01-12 23:02:50'),
(3, 'Info', '2021-01-12 23:02:50'),
(4, 'Blabla', '2021-01-12 23:02:50');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrationsVersion20210112213747', '2021-01-13 21:04:24', 18),
('DoctrineMigrationsVersion20210112214647', '2021-01-13 21:04:24', 1),
('DoctrineMigrationsVersion20210113001347', '2021-01-13 21:04:25', 1),
('DoctrineMigrationsVersion20210113011047', '2021-01-13 21:04:25', 1),
('DoctrineMigrationsVersion20210115205231', '2021-01-15 21:19:14', 20),
('DoctrineMigrationsVersion20210115213050', '2021-01-15 21:31:36', 19),
('DoctrineMigrationsVersion20210117132800', '2021-01-17 14:43:57', 254);

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id` int NOT NULL,
  `id_topic` int NOT NULL,
  `id_user` int NOT NULL,
  `contenu_mess` text NOT NULL,
  `date_heure_mess` datetime NOT NULL,
  `pseudo_user` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `message`
--

INSERT INTO `message` (`id`, `id_topic`, `id_user`, `contenu_mess`, `date_heure_mess`, `pseudo_user`) VALUES
(15, 8, 1, 'L\'important c\'est de perséverer', '2021-01-18 17:09:36', 'Administrateur'),
(16, 8, 2, 'Je te conseille d\'essayer avec un ami, c\'est toujours + fun ;-)', '2021-01-18 18:14:36', 'Utilisateur'),
(18, 12, 2, 'Yes I are !', '2021-01-18 18:20:42', 'Utilisateur'),
(19, 13, 2, 'Je pense que le futur c\'est NodeJS', '2021-01-18 18:21:15', 'Utilisateur'),
(20, 14, 2, 'On est pas un peu seuls ici ?', '2021-01-18 18:21:47', 'Utilisateur'),
(21, 15, 3, 'Bon ça marche mais c\'est tellement lents !', '2021-02-05 21:31:18', 'Jojo');

-- --------------------------------------------------------

--
-- Structure de la table `topic`
--

CREATE TABLE `topic` (
  `id` int NOT NULL,
  `id_cate` int NOT NULL,
  `id_user` int NOT NULL,
  `titre_topic` varchar(255) NOT NULL,
  `contenu_topic` text,
  `date_heure_topic` datetime NOT NULL,
  `pseudo_user` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `topic`
--

INSERT INTO `topic` (`id`, `id_cate`, `id_user`, `titre_topic`, `contenu_topic`, `date_heure_topic`, `pseudo_user`) VALUES
(8, 1, 1, 'Le Skate c\'est bien ?', 'J\'aimerais me mettre au skate mais j\'hésite', '2021-01-18 17:08:34', 'Administrateur'),
(9, 1, 2, 'Une marque de planche ?? conseiller ?', 'J\'ai entendu beaucoup de bien de Magenta, votre avis ?', '2021-01-18 18:15:21', 'Utilisateur'),
(12, 2, 1, 'De l\'aide pour l\'anglais', 'Y\'a t\'il un bilingue ici ?', '2021-01-18 18:18:08', 'Administrateur'),
(13, 3, 1, 'Symfony en 2021 ?', 'N\'est-ce pas démodé ?', '2021-01-18 18:18:31', 'Administrateur'),
(14, 4, 1, 'FAQ', 'Avez-vous des questions ? ', '2021-01-18 18:18:56', 'Administrateur'),
(15, 2, 3, 'Alors ça marche le site sous docker ?', 'Je fais des tests je redis ça', '2021-02-05 21:30:52', 'Jojo');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `username` varchar(180) NOT NULL,
  `roles` text NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `roles`, `password`) VALUES
(1, 'Administrateur', '[\"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$L0ZoUU50Rk1vYnZqSmNYTg$D7XRDVKIyzpYQhfZTxBbRhoM/TeF8Nqc/0THGnnVUds'),
(2, 'Utilisateur', '[]', '$argon2id$v=19$m=65536,t=4,p=1$ODRzWUF3N2NqWC9GZzVjUw$j1Lx01MQU25e1MFVmSWMLhIsiIUuNJOjz1XvLM6AIS0'),
(3, 'Jojo', '[]', '$argon2id$v=19$m=65536,t=4,p=1$pTKRnUVtQG3hyRhNJa/xbA$clf3VtT6mPBGFci+iUmG02cUZRNEr4tdZOmEhEZ4Bvo');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `topic`
--
ALTER TABLE `topic`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
